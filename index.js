const express = require('express');
const bodyParser = require('body-parser');
var hdb = require('@sap/hana-client');
const conf = require('./hdbconf.json');

const client = hdb.createConnection();

const port = 3000;
const app = express();

app.use(bodyParser.text())
app.use(bodyParser.json())

app.post('/', (req, res) => {
	const sql = req.body;
	console.log(sql);
	client.connect(conf);
	try{
		const queryResult = client.exec(sql);
		client.disconnect();
		res.send(queryResult);
	}catch(e){
		client.disconnect();
		console.warn(e.stack);
		const simpleError = {...e};
		delete simpleError.stack;
		res.send(simpleError);
	}
});
app.post('/update', (req, res) => {
	const payload = req.body;
	console.log(payload.statement);
	client.connect(conf);
	try{
		const statement = client.prepare(payload.statement);
		const res = statement.exec(payload.values);
		client.disconnect();
		res.send(res?true:false);
	}catch(e){
		client.disconnect();
		console.warn(e.stack);
		const simpleError = {...e};
		delete simpleError.stack;
		res.send(simpleError);
	}
});
app.listen(port, () => console.log(`Example app listening on port ${port}!`));

