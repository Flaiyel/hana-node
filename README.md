# Installation

## Install dependencies
```
npm config set @sap:registry https://npm.sap.com
npm install
```

## Create configuration files
Name the file `hdbconf.json` on the root of this folder of this proyect

```json
{
    "host": "as1-100-01",
    "instanceNumber": "00",
    "databaseName": "TD1",
    "port": 30013,
    "user": "",
    "password": ""
}
```
# Usage

Use the following command to exeucte a query:
```
# REMEMBER TO LIMIT THE QUERY MANUALLY!!!!
node executeQuery.js < query.sql

```